import 'dart:async';
import 'dart:collection';
import 'package:flutter_phoenix_socket/src/channels_section/channel_event.dart';
import 'package:flutter_phoenix_socket/src/endpoints_section/wrapped_endpoint.dart';
import 'package:flutter_phoenix_socket/src/phoenix_envelope/envelope_timeout_binding.dart';
import 'package:flutter_phoenix_socket/src/phoenix_envelope/phoenix_envelope.dart';
import 'package:flutter_phoenix_socket/src/phoenix_envelope/serializer.dart';
import 'package:logging/logging.dart';
import 'channel_errors.dart' as errors;
import 'wrapped_channel.dart';

final _logger = Logger('ChannelRouter');

class ChannelRouter {
  /// unique reference for each message
  int _ref = 0;
  String get ref => (_ref++).toString();

  /// ref to binding mapping
  var _envelopeTimeOutBindings =
      HashMap<String, PhoenixEnvelopeTimeOutBinding>();

  // topic and wrapped channel
  var _wrappedChannelsTracker = HashMap<String, WrappedChannel>();

  final LifeCycleAwareWrappedEndPoint _wrappedEndPoint;

  ChannelRouter(this._wrappedEndPoint);

  void routeIncomingMessage(dynamic rawData) {
    if (rawData is String) {
      _logger.finest("received $rawData");

      var envelope = EnvelopeSerializer().decode(rawData);

      var envelopeBinding = _envelopeTimeOutBindings[envelope.ref];
      if (envelopeBinding != null) {
        // complete the completer if someone was waiting for this message
        envelopeBinding.completeSuccessfully(envelope);
        _envelopeTimeOutBindings.remove(envelope.ref);
      } else {
        // route to correct channel with the help of topic
        var wrappedChannel = _wrappedChannelsTracker[envelope.topic];
        wrappedChannel?.handleIncomingMessage(envelope);
      }
    } else {
      _logger.severe(
          "raw message expected a string  but got ${rawData.runtimeType} ");
    }
  }

  /// it may return null if user has not joined the channel
  Future<PhoenixEnvelope?>? sendMessageForReply(PhoenixEnvelope envelope,
      {Duration timeout = const Duration(seconds: 5)}) {
    // check if message should be forwarded or not
    if (_shouldSendMessage(envelope)) {
      //encode the envelope
      var encodedEnvelope = EnvelopeSerializer().encode(envelope);
      var completer = Completer<PhoenixEnvelope?>();
      // send real message
      _wrappedEndPoint.webSocket?.sink.add(encodedEnvelope);
      // timer for handling timeout for this message
      Timer timer = Timer(timeout, () => _raiseTimeOut(envelope.ref!));
      _envelopeTimeOutBindings[envelope.ref!] =
          PhoenixEnvelopeTimeOutBinding(completer, timer);
      return completer.future;
    }
  }

  /// checks whether outgoing envelope is of whitelisted
  /// events like room_join, room_leave , heartbeat etc
  /// and if that is not the case then assure that
  /// the  endpoint is joined to that particular topic
  /// before sending any message
  bool _shouldSendMessage(PhoenixEnvelope envelope) {
    if (PhoenixChannelEvent.whitelistEvents.contains(envelope.event)) {
      return true;
    } else {
      var channel = _wrappedChannelsTracker[envelope.topic];
      if (channel != null && channel.channelState == ChannelState.joined) {
        return true;
      }
    }
    return false;
  }

  /// send message without expecting any acknowledgment
  /// or reply
  void sendMessage(PhoenixEnvelope envelope) {
    if (_shouldSendMessage(envelope)) {
      var encodedEnvelope = EnvelopeSerializer().encode(envelope);
      _wrappedEndPoint.webSocket?.sink.add(encodedEnvelope);
    }
  }

  /// callback triggered for raising timeout of messages
  /// which were expecting reply but didn't get any reply
  void _raiseTimeOut(String ref) {
    _envelopeTimeOutBindings[ref]?.raiseTimeOut();
    _envelopeTimeOutBindings.remove(ref);
  }

  /// triggered for clearing the router or resetting it .
  Future<void> dispose() async {
    for (final binding in _envelopeTimeOutBindings.values) {
      _logger.finest("going background so canceling $binding ");
      binding.raiseTimeOut();
    }
    _envelopeTimeOutBindings.clear();
    var allChannelsCloseCalls = <Future>[];

    for (var wrappedChannel in _wrappedChannelsTracker.values) {
      allChannelsCloseCalls.add(wrappedChannel.leave());
    }

    await Future.wait(allChannelsCloseCalls);

    _wrappedChannelsTracker.clear();
    _ref = 0;
  }

  /// add new channel
  void addChannel(String topic) {
    _wrappedChannelsTracker.putIfAbsent(topic, () {
      var phoenixChannel = _wrappedEndPoint.endPoint.getChannel(topic);
      return WrappedChannel(phoenixChannel, topic, this);
    });
  }

  /// join new channel if that channel is not added
  /// then it will internally add it first then try to join
  void joinChannel(String topic) {
    var wrappedChannel = _wrappedChannelsTracker[topic];
    if (wrappedChannel != null) {
      wrappedChannel.join();
    } else {
      addChannel(topic);
      joinChannel(topic);
    }
  }

  /// called if some error occurred in underlying socket
  /// so it will change state of all channels to errored and
  /// fire timeout for any expected replies
  void propagateError(Object error, StackTrace stacktrace) {
    for (final binding in _envelopeTimeOutBindings.values) {
      _logger.finest("going background so canceling $binding ");
      binding.raiseTimeOut();
    }
    _envelopeTimeOutBindings.clear();

    for (final wrappedChannel in _wrappedChannelsTracker.values) {
      _logger.finest("going background so closing $wrappedChannel ");
      wrappedChannel.channelErrored(errors.socketErrored);
    }
    _ref = 0;
  }
}
