import 'package:flutter/foundation.dart';
import 'package:flutter_phoenix_socket/src/channels_section/channel_event.dart';
import 'package:flutter_phoenix_socket/src/phoenix_envelope/phoenix_envelope.dart';

abstract class PhoenixChannel {
  Duration joinRequestTimeoutDuration;

  @nonVirtual
  PhoenixChannelRouterCommunicator? _privateChannelCommunicator;

  set privateChannelCommunicator(
          PhoenixChannelRouterCommunicator communicator) =>
      this._privateChannelCommunicator = communicator;

  PhoenixChannel(
      {this.joinRequestTimeoutDuration = const Duration(seconds: 5)});

  /// callback to trigger after joining
  void channelJoined(String completeTopic);

  /// callback to trigger after successfully leaving
  /// you can place your cleanup code here
  void channelLeaved(String completeTopic);

  /// callback to trigger on new message arriving from server
  void onNewMessage(Map<String, dynamic> message);

  /// callback tp trigger on channel error
  void onChannelError(PhoenixChannelError channelError);

  Map<String, dynamic> getParamsForChannel(String completeTopic);

  @nonVirtual
  Future<void> join() => _privateChannelCommunicator!.join();

  @nonVirtual
  Future<void> leave() => _privateChannelCommunicator!.leave();

  @nonVirtual
  void sendPhoenixMessage(String event, Map<String, dynamic> payload) =>
      _privateChannelCommunicator?.sendPhoenixMessage(event, payload);

  @nonVirtual
  Future<PhoenixEnvelope?>? sendPhoenixMessageForReply(
          String event, Map<String, dynamic> payload) =>
      _privateChannelCommunicator?.sendPhoenixMessageForReply(event, payload);
}

abstract class PhoenixChannelRouterCommunicator {
  void sendPhoenixMessage(String event, Map<String, dynamic> payload);

  Future<PhoenixEnvelope?>? sendPhoenixMessageForReply(
      String event, Map<String, dynamic> payload);

  Future<void> leave();

  Future<void> join();
}
