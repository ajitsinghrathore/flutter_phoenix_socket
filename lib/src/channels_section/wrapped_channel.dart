import 'package:flutter_phoenix_socket/src/channels_section/channel.dart';
import 'package:flutter_phoenix_socket/src/channels_section/channel_event.dart';
import 'package:flutter_phoenix_socket/src/channels_section/channel_router.dart';
import 'package:flutter_phoenix_socket/src/phoenix_envelope/phoenix_envelope.dart';
import 'package:logging/logging.dart';

import 'channel_errors.dart' as errors;

final _logger = Logger('ChannelRouter');

class WrappedChannel implements PhoenixChannelRouterCommunicator {
  ChannelState _channelState = ChannelState.closed;
  ChannelState get channelState => _channelState;

  ChannelRouter _router;
  ChannelRouter get router => _router;

  String _topic;

  PhoenixChannel _phoenixChannel;

  bool _shouldLeaveAfterJoining = false;
  bool _shouldJoinAfterLeaving = false;

  WrappedChannel(this._phoenixChannel, this._topic, this._router) {
    _phoenixChannel.privateChannelCommunicator = this;
  }

  void handleIncomingMessage(PhoenixEnvelope message) {
    if (message.event == PhoenixChannelEvent.error) {
      channelErrored(errors.channelErrorReceivedFromServer);
    } else {
      // phx_join and phx_leave are handled internally in such a way that they
      // will never reach here as they will get filter out in router only
      //because there will be some completer associated with them
      //phx_error is handled in upper if
      //phx_reply
      _phoenixChannel.onNewMessage(message.payload);
    }
  }

  void _updateChannelState(ChannelState state) {
    _channelState = state;
  }

  void _channelJoined() {
    _updateChannelState(ChannelState.joined);
    if (_shouldLeaveAfterJoining) {
      _shouldLeaveAfterJoining = false;
      leave();
    } else {
      _phoenixChannel.channelJoined(_topic);
    }
  }

  void _channelLeaved() {
    _updateChannelState(ChannelState.closed);
    if (_shouldJoinAfterLeaving) {
      _shouldJoinAfterLeaving = false;
      join();
    } else {
      _phoenixChannel.channelLeaved(_topic);
    }
  }

  void channelErrored(int code) {
    _updateChannelState(ChannelState.errored);
    _phoenixChannel.onChannelError(PhoenixChannelError(code));
  }

  bool _checkPositiveStateOfReply(PhoenixEnvelope envelope) {
    return envelope.payload['status'] != null &&
        envelope.payload['status'] == "ok";
  }

  //   methods used to communicate to channels api

  @override
  void sendPhoenixMessage(String event, Map<String, dynamic> payload) {
    var envelope = PhoenixEnvelope(
        topic: _topic,
        event: PhoenixChannelEvent.custom(event),
        ref: router.ref,
        payload: payload);
    router.sendMessage(envelope);
  }

  @override
  Future<PhoenixEnvelope?>? sendPhoenixMessageForReply(
      String event, Map<String, dynamic> payload) {
    var envelope = PhoenixEnvelope(
        topic: _topic,
        event: PhoenixChannelEvent.custom(event),
        ref: router.ref,
        payload: payload);
    return router.sendMessageForReply(envelope);
  }

  Future<void> leave() async {
    if (channelState == ChannelState.joined) {
      var envelope =
          PhoenixEnvelope.channelLeave(ref: router.ref, topic: _topic);
      _updateChannelState(ChannelState.leaving);
      var reply = await router.sendMessageForReply(envelope);
      if (reply != null) {
        if (_checkPositiveStateOfReply(reply))
          _channelLeaved();
        else
          channelErrored(errors.leaveRequestReceivedError);
      } else {
        channelErrored(errors.leaveRequestTimeOut);
      }
    } else if (channelState == ChannelState.joining) {
      _shouldLeaveAfterJoining = true;
    }
  }

  Future<void> join() async {
    if (channelState == ChannelState.closed ||
        channelState == ChannelState.errored) {
      var envelope = PhoenixEnvelope.channelJoin(
          ref: router.ref,
          topic: _topic,
          payload: _phoenixChannel.getParamsForChannel(_topic));
      _updateChannelState(ChannelState.joining);
      var joinReply = await router.sendMessageForReply(envelope);
      if (joinReply != null) {
        if (_checkPositiveStateOfReply(joinReply)) {
          _channelJoined();
        } else {
          channelErrored(errors.joinRequestDenied);
        }
      } else {
        join();
      }
    } else if (channelState == ChannelState.leaving) {
      _shouldJoinAfterLeaving = true;
    } else {
      _logger.warning(" channel -> $_topic  is already $_channelState");
    }
  }
}
