class PhoenixChannelError {
  int code;

  PhoenixChannelError(this.code);
}

enum ChannelState {
  /// The channel is closed, after a normal leave.
  closed,

  /// The channel is errored.
  errored,

  /// The channel is joined and functional.
  joined,

  /// The channel is waiting for a reply to its 'join'
  /// request.
  joining,

  /// The channel is waiting for a reply to its 'leave'
  /// request.
  leaving,
}

class PhoenixChannelEvent {
  PhoenixChannelEvent._(this.value);

  /// A custom push event.
  ///
  /// This is the event name used when a user of the library sends a message
  /// on a channel.
  factory PhoenixChannelEvent.custom(name) => PhoenixChannelEvent._(name);

  /// Instantiates a PhoenixChannelEvent from
  /// one of the values used in the wire protocol.
  factory PhoenixChannelEvent(String value) {
    switch (value) {
      case __closeEventName:
        return close;
      case __errorEventName:
        return error;
      case __joinEventName:
        return join;
      case __replyEventName:
        return reply;
      case __leaveEventName:
        return leave;
      default:
        throw ArgumentError.value(value);
    }
  }
  static const String __closeEventName = 'phx_close';
  static const String __errorEventName = 'phx_error';
  static const String __joinEventName = 'phx_join';
  static const String __replyEventName = 'phx_reply';
  static const String __leaveEventName = 'phx_leave';

  /// The string value for a channel event.
  final String value;

  /// The constant close event
  static PhoenixChannelEvent close = PhoenixChannelEvent._(__closeEventName);

  /// The constant error event
  static PhoenixChannelEvent error = PhoenixChannelEvent._(__errorEventName);

  /// The constant join event
  static PhoenixChannelEvent join = PhoenixChannelEvent._(__joinEventName);

  /// The constant reply event
  static PhoenixChannelEvent reply = PhoenixChannelEvent._(__replyEventName);

  /// The constant leave event
  static PhoenixChannelEvent leave = PhoenixChannelEvent._(__leaveEventName);

  static PhoenixChannelEvent heartbeat = PhoenixChannelEvent._('heartbeat');

  /// The constant set of possible internal channel event names.
  static Set<PhoenixChannelEvent> whitelistEvents = {
    close,
    error,
    join,
    reply,
    leave,
    heartbeat
  };
}
