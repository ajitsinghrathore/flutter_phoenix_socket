import 'package:flutter/widgets.dart';
import 'package:flutter_phoenix_socket/src/endpoints_section/endpoint.dart';
import 'package:flutter_phoenix_socket/src/endpoints_section/wrapped_endpoint.dart';
import 'package:logging/logging.dart';

final _logger = Logger('EndPointsManager');

class EndPointsManager with WidgetsBindingObserver {
  static final EndPointsManager manager = EndPointsManager._internal();

  factory EndPointsManager() {
    return manager;
  }

  EndPointsManager._internal();

  var _endpointTracker = <String, LifeCycleAwareWrappedEndPoint>{};

  void startAllServices() {
    WidgetsBinding.instance?.addObserver(this);
    var allEndPointIterator = all();
    while (allEndPointIterator.moveNext()) {
      allEndPointIterator.current.startEndPoint();
    }
  }

  void stopAllServices() {
    WidgetsBinding.instance?.removeObserver(this);
    var allEndPointIterator = all();
    while (allEndPointIterator.moveNext()) {
      allEndPointIterator.current.stopEndPoint();
    }
  }

  void startService(String serviceName) {
    _endpointTracker[serviceName]!.startEndPoint();
  }

  void stopService(String serviceName) {
    _endpointTracker[serviceName]!.stopEndPoint();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    bool? isBackground;
    switch (state) {
      case AppLifecycleState.resumed:
        isBackground = false;
        break;

      case AppLifecycleState.paused:
        isBackground = true;
        break;
      case AppLifecycleState.inactive:
      case AppLifecycleState.detached:
    }
    if (isBackground != null) {
      _logger
          .info("application state changed so notifying to all the endpoints");
      var allEndPointIterator = all();
      while (allEndPointIterator.moveNext()) {
        allEndPointIterator.current.appStateChanged(isBackground);
      }
    }
  }

  LifeCycleAwareWrappedEndPoint? getEndPoint(String serviceName) {
    return _endpointTracker[serviceName];
  }

  void addNewEndPoint(PhoenixEndPoint endpoint, {bool startNow = true}) {
    _endpointTracker.putIfAbsent(
        endpoint.serviceName, () => LifeCycleAwareWrappedEndPoint(endpoint));
  }

  Iterator<LifeCycleAwareWrappedEndPoint> all() {
    return _endpointTracker.values.iterator;
  }
}
