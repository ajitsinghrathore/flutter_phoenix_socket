import 'package:flutter_phoenix_socket/src/channels_section/channel_event.dart';

class PhoenixEnvelope {
  final String? ref;
  final String topic;
  final PhoenixChannelEvent event;
  final Map<String, dynamic> payload;

  PhoenixEnvelope(
      {required this.topic,
      required this.event,
      required this.ref,
      required this.payload});

  PhoenixEnvelope.heartbeat({required this.ref})
      : this.event = PhoenixChannelEvent.heartbeat,
        this.topic = "phoenix",
        this.payload = {};

  PhoenixEnvelope.channelJoin(
      {required this.ref, required this.topic, required this.payload})
      : this.event = PhoenixChannelEvent.join;

  PhoenixEnvelope.channelLeave({required this.ref, required this.topic})
      : this.event = PhoenixChannelEvent.leave,
        this.payload = {};

  factory PhoenixEnvelope.fromJson(Map<String, dynamic> json) {
    return PhoenixEnvelope(
      topic: json['topic'],
      ref: json['ref'],
      event: PhoenixChannelEvent.custom(json['event']),
      payload: json['payload'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "ref": this.ref,
      "topic": this.topic,
      "event": this.event.value,
      "payload": this.payload,
    };
  }
}
