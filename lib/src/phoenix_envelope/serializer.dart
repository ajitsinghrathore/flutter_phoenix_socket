import 'dart:convert';

import 'package:flutter_phoenix_socket/src/phoenix_envelope/phoenix_envelope.dart';

class EnvelopeSerializer {
  EnvelopeSerializer._();

  /// Default constructor returning the singleton instance of this class.
  factory EnvelopeSerializer() => _instance ??= EnvelopeSerializer._();

  static EnvelopeSerializer? _instance;

  PhoenixEnvelope decode(String rawData) {
    return PhoenixEnvelope.fromJson(jsonDecode(rawData));
  }

  String encode(PhoenixEnvelope envelope) {
    return jsonEncode(envelope.toJson());
  }
}
