import 'dart:async';

import 'package:flutter_phoenix_socket/src/phoenix_envelope/phoenix_envelope.dart';

class PhoenixEnvelopeTimeOutBinding {
  Completer<PhoenixEnvelope?>? completer;
  Timer? timer;

  PhoenixEnvelopeTimeOutBinding(this.completer, this.timer);

  void completeSuccessfully(PhoenixEnvelope envelope) {
    if (isCompleted()) {
      return;
    }
    this.timer?.cancel();
    this.completer?.complete(envelope);
    this.completer = null;
    this.timer = null;
  }

  void raiseTimeOut() {
    if (isCompleted()) {
      return;
    }
    this.timer?.cancel();
    completer?.complete(null);
    this.timer = null;
    this.completer = null;
  }

  bool isCompleted() {
    return completer == null;
  }
}
