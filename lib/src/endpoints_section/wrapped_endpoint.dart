import 'dart:async';
import 'package:flutter_phoenix_socket/src/channels_section/channel_router.dart';
import 'package:flutter_phoenix_socket/src/endpoints_section/endpoint.dart';
import 'package:flutter_phoenix_socket/src/phoenix_envelope/phoenix_envelope.dart';
import 'package:web_socket_channel/io.dart';
import 'endpoint_events.dart';
import 'package:logging/logging.dart';
import 'package:web_socket_channel/status.dart' as status;

final _logger = Logger('LifeCycleAwareWrappedEndPoint');

class LifeCycleAwareWrappedEndPoint {
  /// [EndPointState] current state of endPoint
  EndPointState _webSocketState = EndPointState.closed;
  EndPointState get webSocketState => _webSocketState;

  /// [IOWebSocketChannel] underlying socket object
  IOWebSocketChannel? _webSocket;
  IOWebSocketChannel? get webSocket => _webSocket;

  /// [PhoenixEndPoint] endPoint object
  ///  which contains all the necessary configurations
  final PhoenixEndPoint _endpoint;
  PhoenixEndPoint get endPoint => _endpoint;

  /// [whitelisted] channel router for routing incoming and outgoing messages
  ChannelRouter? _channelRouter;
  ChannelRouter? get channelRouter => _channelRouter;

  /// timer for reconnecting the connection
  Timer? _reconnectTimer;

  /// timer for scheduling heartbeat messages
  Timer? _heartbeatTimer;

  /// required as internally on error occurred after socket is open it
  /// will close the socket then we need to reconnect it
  /// so this flag will help in differentiating between explicit close
  /// called via [close({int? code, String? reason})] or
  /// implicit close by underlying socket
  bool _shouldTryReconnecting = true;

  bool _shouldCloseSocket = false;

  /// see this issue
  /// [https://gitlab.com/ajitsinghrathore/flutter_phoenix_socket/-/issues/3]
  bool _waitingForFirstHeartbeat = false;

  LifeCycleAwareWrappedEndPoint(this._endpoint) {
    this._channelRouter = ChannelRouter(this);
  }

  void _connect() {
    if (webSocketState == EndPointState.closing) {
      _shouldTryReconnecting = true;
      _logger
          .info("websocket is closing so will retry after completely closed");
      return;
    }

    if (webSocketState != EndPointState.closed) {
      _logger.warning("""websocket of ${_endpoint.serviceName} is not closed 
          so skipping connect request""");
      return;
    }
    try {
      _cancelReconnectTimer();
      _stopHeartBeatTimer();
      //i am trying to connect but if i fail then please try reconnecting
      _shouldTryReconnecting = true;
      _updateWebSocketState(EndPointState.connecting);
      _webSocket = IOWebSocketChannel.connect(_endpoint.url);
      _webSocket!.stream
          .listen(_channelRouter?.routeIncomingMessage, cancelOnError: true)
            ..onError(_onSocketError)
            ..onDone(_onSocketClosed);
      if (!_waitingForFirstHeartbeat) _startHeartBeatTimer();
    } catch (error, stacktrace) {
      _onSocketError(error, stacktrace);
    }
  }

  void _firstHeartBeatReceived() {
    _waitingForFirstHeartbeat = false;
    _updateWebSocketState(EndPointState.connected);
    if (_shouldCloseSocket) {
      _shouldCloseSocket = false;
      _close();
    } else {
      _endpoint.onConnect();
    }
  }

  void _startReconnectionTimer(Duration reconnectTimeInterval) {
    if (_reconnectTimer != null) {
      _cancelReconnectTimer();
    }
    _logger.info("starting reconnection timer of ${_endpoint.serviceName}");
    _reconnectTimer = Timer.periodic(reconnectTimeInterval, (timer) {
      _logger.info("trying to reconnect ${_endpoint.serviceName}");
      _connect();
    });
  }

  void _cancelReconnectTimer() {
    if (_reconnectTimer != null) {
      _reconnectTimer?.cancel();
      _reconnectTimer = null;
      _logger.info("cancelled reconnect timer of ${_endpoint.serviceName}");
    }
  }

  void _updateWebSocketState(EndPointState state) {
    this._webSocketState = state;
  }

  void _onSocketError(Object error, StackTrace stacktrace) {
    if (webSocketState == EndPointState.closing ||
        webSocketState == EndPointState.closed) {
      _logger
          .warning("""websocket of ${_endpoint.serviceName} is closing or closed
           so skipping error event""");
      return;
    }
    _shouldTryReconnecting = true;
    var event = EndPointErrorEvent(error: error, stacktrace: stacktrace);
    _endpoint.onError(event);
    channelRouter?.propagateError(error, stacktrace);
    _onSocketClosed(fireCloseEvent: false);
  }

  void _onSocketClosed({bool fireCloseEvent = true}) {
    if (webSocketState == EndPointState.closed) {
      _logger.warning("""websocket ${_endpoint.serviceName} 
          is already closed so skipping event propagation""");
      return;
    }
    _stopHeartBeatTimer();
    _updateWebSocketState(EndPointState.closed);
    if (fireCloseEvent) {
      var event = EndPointCloseEvent(
          code: _webSocket!.closeCode, reason: _webSocket!.closeReason);
      _endpoint.onClose(event);
    }
    _webSocket = null;
    if (_shouldTryReconnecting) {
      _startReconnectionTimer(_endpoint.reconnectTimeInterval);
    }
  }

  void _close({int? code, String? reason}) async {
    _shouldTryReconnecting = false;
    if (webSocketState == EndPointState.connecting) {
      _shouldCloseSocket = true;
      _logger.info(
          "endpoint is in connecting state so it will be closed once it opens");
      return;
    }
    if (webSocketState == EndPointState.connected) {
      _updateWebSocketState(EndPointState.closing);
      await _channelRouter?.dispose();
      _webSocket!.sink.close(code, reason);
      _logger.info("websocket closed  of ${_endpoint.serviceName} on request");
    } else {
      _logger.warning("""websocket is not connected
           so skipping close request of ${_endpoint.serviceName}""");
    }
  }

  void _startHeartBeatTimer() async {
    _waitingForFirstHeartbeat = true;
    if (_heartbeatTimer != null) {
      _stopHeartBeatTimer();
    }
    var envelope = PhoenixEnvelope.heartbeat(ref: _channelRouter!.ref);
    var replyHeartbeat = await _channelRouter?.sendMessageForReply(envelope);
    if (replyHeartbeat == null) {
      _startHeartBeatTimer();
    } else {
      _firstHeartBeatReceived();
      _heartbeatTimer =
          Timer.periodic(_endpoint.heartBeatTimeInterval, _sendHeartBeat);
      _logger.finest("heartbeat started");
    }
  }

  void _sendHeartBeat(Timer? timer) {
    var envelope = PhoenixEnvelope.heartbeat(ref: _channelRouter!.ref);
    _channelRouter?.sendMessage(envelope);
    _logger.finest("heartbeat sended");
  }

  void _stopHeartBeatTimer() {
    if (_heartbeatTimer != null) {
      _heartbeatTimer?.cancel();
      _heartbeatTimer = null;
      _logger.finest("heartbeat cancelled");
    }
  }

  void startEndPoint() {
    _connect();
  }

  void stopEndPoint({int? code, String? reason}) {
    _close(code: code, reason: reason);
  }

  void appStateChanged(bool goneBackground) {
    if (goneBackground) {
      if (_endpoint.backgroundAllowed) {
        startEndPoint();
      } else {
        stopEndPoint(code: status.goingAway, reason: "app goes in background");
      }
    } else {
      startEndPoint();
    }
  }
}
