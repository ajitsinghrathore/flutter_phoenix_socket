/// base event for all endPoint events
class EndPointEvent {}

/// event when socket of endPoint is connected
class EndPointOpenEvent extends EndPointEvent {}

/// event when socket of  endPoint is closed
class EndPointCloseEvent extends EndPointEvent {
  EndPointCloseEvent({
    this.reason,
    this.code,
  });

  /// The reason the socket was closed.
  final String? reason;

  /// The code of the socket close.
  final int? code;
}

/// event when some error occurred in  endPoint
class EndPointErrorEvent extends EndPointEvent {
  /// Default constructor for the error event.
  EndPointErrorEvent({
    this.error,
    this.stacktrace,
  });

  /// The error that happened on the socket
  final dynamic error;

  /// The stacktrace associated with the error.
  final dynamic stacktrace;
}

/// State of a endPoint.
enum EndPointState {
  /// The connection is closed
  closed,

  /// The connection is closing
  closing,

  /// The connection is opening
  connecting,

  /// The connection is established
  connected,
}
