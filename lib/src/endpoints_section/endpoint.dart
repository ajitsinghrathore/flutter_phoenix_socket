import 'package:flutter/foundation.dart';
import 'package:flutter_phoenix_socket/src/endpoints_manager.dart';

import 'package:flutter_phoenix_socket/src/channels_section/channel.dart';
import 'endpoint_events.dart';

abstract class PhoenixEndPoint {
  String serviceName;
  String url;
  Duration reconnectTimeInterval;
  bool backgroundAllowed;
  Duration heartBeatTimeInterval;

  PhoenixEndPoint(
    this.serviceName,
    this.url, {
    this.reconnectTimeInterval = const Duration(seconds: 5),
    this.backgroundAllowed = false,
    this.heartBeatTimeInterval = const Duration(seconds: 15),
  });

  Future<void> onConnect();
  Future<void> onClose(EndPointCloseEvent closeEvent);
  Future<void> onError(EndPointErrorEvent errorEvent);

  @nonVirtual
  EndPointState? currentEndPointState() {
    return EndPointsManager().getEndPoint(serviceName)?.webSocketState;
  }

  PhoenixChannel getChannel(String topic);
}
