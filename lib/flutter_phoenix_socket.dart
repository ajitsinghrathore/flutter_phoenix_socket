library flutter_phoenix_socket;

import 'package:flutter_phoenix_socket/src/channels_section/channel_event.dart';
import 'package:flutter_phoenix_socket/src/endpoints_manager.dart';
import 'package:flutter_phoenix_socket/src/phoenix_envelope/phoenix_envelope.dart';

export 'src/endpoints_manager.dart';
export 'src/endpoints_section/endpoint.dart';
export 'src/endpoints_section/endpoint_events.dart';
export 'src/endpoints_section/wrapped_endpoint.dart';
export 'src/channels_section/channel.dart';
export 'src/channels_section/channel_event.dart';
export 'src/channels_section/channel_router.dart';
export 'src/channels_section/wrapped_channel.dart';

void phoenixAddNewChannel(String endPointServiceName, String channelTopic) {
  var wrappedEndPoint = EndPointsManager().getEndPoint(endPointServiceName);
  wrappedEndPoint?.channelRouter?.addChannel(channelTopic);
}

void phoenixJoinChannel(String endPointServiceName, String channelTopic) {
  var wrappedEndPoint = EndPointsManager().getEndPoint(endPointServiceName);
  wrappedEndPoint?.channelRouter?.joinChannel(channelTopic);
}

void sendPhoenixMessage(String serviceName, String topic, String event,
    Map<String, dynamic> payload) {
  var wrappedEndPoint = EndPointsManager().getEndPoint(serviceName);
  var router = wrappedEndPoint?.channelRouter;
  var envelope = PhoenixEnvelope(
      topic: topic,
      event: PhoenixChannelEvent.custom(event),
      ref: router!.ref,
      payload: payload);
  wrappedEndPoint?.channelRouter?.sendMessage(envelope);
}

Future<PhoenixEnvelope?>? sendPhoenixMessageForRely(String serviceName,
    String topic, String event, Map<String, dynamic> payload,
    {Duration replyTimeOut = const Duration(seconds: 5)}) {
  var wrappedEndPoint = EndPointsManager().getEndPoint(serviceName);
  var router = wrappedEndPoint?.channelRouter;
  var envelope = PhoenixEnvelope(
      topic: topic,
      event: PhoenixChannelEvent.custom(event),
      ref: router!.ref,
      payload: payload);
  return wrappedEndPoint?.channelRouter
      ?.sendMessageForReply(envelope, timeout: replyTimeOut);
}
