Below are the configuration variables which you need to either provide in constructor or override  these variables int your endpoint class to  configure endpoints. 

### joinRequestTimeoutDuration
   - type : Duration  
   - required : optional  &nbsp; &nbsp; default : Duration(seconds: 5)
   - purpose :  this is the time after which a join request will be considered fails if there is no reply from server


## callbacks

below are some  callbacks and methods which need to be overridden while extending PhoenixEndPoint


### channelJoined 
   - params : 
       - String completeTopic 

triggered whenever channel is joined successfully .

### channelLeaved 
   - params :
      - String completeTopic 

triggered after successfully leaving.  you can place your cleanup code here

### onNewMessage
   - params :
      - Map<String, dynamic> message   

triggered whenever any message is received 

### onChannelError 
   - params :
      - PhoenixChannelError channelError (contains code for error)

triggered whenever any occurs in channels

### getParamsForChannel
   - params :
      - String completeTopic
   - return type 
      - Map<String, dynamic>   

this method is  used to get any extra data for the join request of channel whenever  joinChannel is called  for a channel . this is the place where you should return credentials for joining to a particular channel




