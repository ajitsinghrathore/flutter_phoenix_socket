Below are the configuration variables which you need to either provide in constructor or override  these variables int your endpoint class to  configure endpoints. 

### serviceName 
   - type : String  
   - required : true
   - purpose : Used to uniquely identify endpoint(websocket) 

### url
   - type : String  
   - required : true
   - purpose : Url string of the  endpoint(websocket) to connect

### reconnectTimeInterval
   - type : Duration  
   - required : optional  &nbsp; &nbsp; default : Duration(seconds: 5)
   - purpose :  used to try reconnecting after this time interval if any error occurred  in endpoint 

### backgroundAllowed
   - type : bool  
   - required : optional  &nbsp; &nbsp; default : false
   - purpose :  indicates whether  this endpoint should maintain its connection in background also. *Note that due to background connection limitation in most android devices this may not work* .


### heartBeatTimeInterval
   - type : Duration  
   - required : optional  &nbsp; &nbsp; default : Duration(seconds: 15)
   - purpose :  used to send heartbeats to phoenix server at this interval . tweak this according to your requirement 


## callbacks

below are some  callbacks and methods which need to be overridden while extending PhoenixEndPoint

### onConnect 
   - params : None

triggered whenever connection is established successfully .

### onClose 
   - params :
      - EndPointCloseEvent ( contains code and reason )

triggered whenever connection closes 

### onError
   - params :
      - EndPointErrorEvent ( contains code and reason )

triggered whenever any error occurs in endpoint

### getChannel 
   - params :
      - String topic
   - return type 
      - PhoenixChannel  

this method is  used to get channel object whenever  addChannel is called  for an endpoint . it should return new channel object(extended from PhoenixChannel) for given topic 


