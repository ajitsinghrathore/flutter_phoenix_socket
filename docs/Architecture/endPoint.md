
Endpoints are just wrapper over websocket. Lets say  a app requires to maintain multiple websocket connection then each websocket connection is a endpoint and each endpoint  can be configured differently by registering object of class that extends PhoenixEndPoint  to EndPointsManager 

Lets consider we need two websocket connections  connected to different servers then there are two ways to achieve this 

1. make two separate classes for each websocket connection 
2. make one class for both websocket connections

first approach is more suitable if you want to keep all the logic related to each websocket different  and add your own custom logic of how this endpoint will  interact with your app .

second approach is more suitable if we  want to  open two connections to the same server  so that the logic can be generalized  for all the connections 


approach 1.
```
class EndPoint1 extends PhoenixEndPoint{
        EndPoint1() : super("connectionOneService", "connectionOneUrl");
        ............  rest of the code 
}

class EndPoint2 extends PhoenixEndPoint{
        EndPoint2() : super("connectionTwoService", "connectionTwoUrl");
        ............  rest of the code 
}

void main() {
  runApp(MyApp());
  EndPointsManager().addNewEndPoint(EndPoint1());
  EndPointsManager().addNewEndPoint(EndPoint2());
  EndPointsManager().startAllServices();
}

```


approach 2 .

```
class MyEndPoint extends PhoenixEndPoint{
        MyEndPoint(String serviceName , String url) : super(serviceName, url);
        ............  rest of the code 
}


void main() {
  runApp(MyApp());
  EndPointsManager().addNewEndPoint(MyEndPoint("serviceOne","url1");
  EndPointsManager().addNewEndPoint(MyEndPoint("serviceTwo","url2");
  EndPointsManager().startAllServices();
}

```


 
Whenever in your code if you need  to access these endpoints object use  ```EndPointsManager()getEndPoint(String serviceName)``` method.




---
**NOTE**

*EndPoints are lifecycleAware so you don't need to worry about closing and reconnecting whenever app goes in background and again coming  to foreground library will handle all those cases according to the configuration for each endpoint*

--- 