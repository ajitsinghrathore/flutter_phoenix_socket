After defining both endpoint class and  channels , now you need to provide the necessary endpoint in flutter main function

```dart
import 'package:flutter_phoenix_socket/flutter_phoenix_socket.dart';

void main() {
  
  runApp(MyApp());
  EndPointsManager().addNewEndPoint(
      MyEndPoint("chatService", "ws://127.0.0.1:4000/socket/websocket"));
  EndPointsManager().startAllServices();
}

```



---
**NOTE**

do not call ```EndPointsManager().startAllServices();```  before ```runApp();``` as library will internally  observer the lifecycle of app so it needs to be started after running app

--- 