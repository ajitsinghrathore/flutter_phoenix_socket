
EndPoints are WebSocket connection and in order to  create websocket connection 
we need to  extend [PhoenixEndPoint](/Architecture/endPoint/) and provide necessary configuration like endpoint url , service name etc .




```dart
import 'package:flutter_phoenix_socket/flutter_phoenix_socket.dart';

class MyEndPoint extends PhoenixEndPoint {
  // this flag indicates whether to keep connection open in background or not
  var backgroundAllowed = false;

  // serviceName is unique name given to your endpoint to differentiate 
  // between multiple endpoints 
  // url is websocket url to connect to ( in ws or wss only)
  MyEndPoint(String serviceName, String url) : super(serviceName, url);


  @override
  Future<void> onClose(EndPointCloseEvent closeEvent) async {
    print("my closed ${closeEvent.reason}");
    // fired when endpoint is closed here you can put  your cleanUp code specific to this endpoint 
  }

  @override
  Future<void> onConnect() async {
    print("my connected");
    // fired when connected successfully
    // join a channel with topic "chat:incoming"
    phoenixJoinChannel(serviceName, "chat:incoming");
    
  }

  @override
  Future<void> onError(EndPointErrorEvent errorEvent) async {
    print("my error occurred ${errorEvent.error}");
    // triggered if some error occurred in  websocket
  }

  @override
  PhoenixChannel getChannel(String topic) {

    // TODO return your new instance of channel which we will be  defining in next section 
    
  }
}

```