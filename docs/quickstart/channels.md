channels are abstraction over webSocket in phoenix in order to communicate to server you must communicate through a channel and for that you must add the channel then join it so that you can start exchanging messages through that channel

```
import 'package:flutter_phoenix_socket/flutter_phoenix_socket.dart';

class MyChannel extends PhoenixChannel {
  // joinRequestTimeoutDuration is timeout for joining request 
  MyChannel(Duration joinRequestTimeoutDuration)
      : super(joinRequestTimeoutDuration: joinRequestTimeoutDuration);

  @override
  void channelJoined(String completeTopic) {
    print("my channel joined $completeTopic");
    this.sendPhoenixMessage("ping", {"key1": "value1", "key2": "value2"});
  }

  @override
  void channelLeaved(String completeTopic) {
    print("my channel leaved $completeTopic");
  }

  @override
  void onNewMessage(Map<String, dynamic> message) {
    print(" my $message");
  }

  @override
  void onChannelError(PhoenixChannelError channelError) {
    print("my  error $channelError");
  }

  @override
  Map<String, dynamic> getParamsForChannel(String completeTopic) {
    // this method is called  internally before join request for 
    // any channel of this class is send  and it attaches it to parameters 
    // this is the  place where you should return  any credentials for verification of channel 
    // join request  
    return {"user": "username"};

  }
}

```