These api calls are  context free  so you can call these from anywhere in your code just by importing the library 

### **phoenixAddNewChannel**

- *return Type* : void
- *parameters* : 
    - required :
        1. String endPointServiceName  
        2. String channelTopic

This call is used to add new channel to given *endPointServiceName* with given *topic*, it will just add the channel without joining it.

### **phoenixJoinChannel**

- *return Type* : void
- *parameters* : 
    - required :
        1. String endPointServiceName  
        2. String channelTopic

context free version of [join()](/api/channels/#join)

This call is used to join to  given *channelTopic* in given *endPointServiceName* ,  If the channel was not added then it will first add the channel then will try to join it .

### **sendPhoenixMessage**

- *return Type* : void
- *parameters* : 
    - required :
        1. String serviceName  
        2. String topic
        3. String event
        4. Map<String, dynamic\> payload

context free version of [send()](/api/channels/#send) as this can be called from anywhere so you need to supply the endpoint by giving its *serviceName*


### **sendPhoenixMessageForReply**

- *return Type* : Future<Map<String, dynamic\>?\>?
- *parameters* : 
    - required :
        1. String serviceName  
        2. String topic
        3. String event
        4. Map<String, dynamic\> payload
    - optional 
        1. Duration replyTimeOut  &nbsp; &nbsp;    default : Duration(seconds :5)  

context free version of [sendForReply()](/api/channels/#sendForReply) 


    
