These api calls are specific to classes extending [PhoenixChannel]()

###  **join**<a name="join"></a>
- *return Type* : Future<void\>
- *parameters* : 
    - optional 
        1. bool retryAfterJoinRequestTimeOut  &nbsp; &nbsp;    default : true

This is used to join the channel from which it is being called . it return a future which will complete with null after it joins successfully . If your backend has not authorize  then it will trigger onChannelError  with  [joinRequestDenied](/api/error_codes/#joinRequestDenied) error code and if timeout occurs then according to the parameter retryAfterJoinRequestTimeOut it will retry joining else it will return [joinRequestTimeOut](/api/error_codes/#joinRequestTimeOut) error code in onChannelError callback.

```
    class MyChannel extends PhoenixChannel {  
        ........... other code
        @override
        void onChannelError(PhoenixChannelError channelError) {
                print("my  error $channelError");
                // this will try to join again if any error occurred 
                join();

        }
        ............. your other code  
   }
```

### **leave**<a name="leave"></a>

- *return Type* : Future<void\>
- *parameters* : None

This is used to leave the channel from which it is being called . it will return future which will complete with null after it leaves successfully and if it fails with any two reason listed below it will trigger onChannelError  with appropriate error code

1. if leave request TimeOuts then it will trigger with  [leaveRequestTimeOut](/api/error_codes/#leaveRequestTimeOut)
2. if server responded without ok then it will trigger with [leaveRequestReceivedError](/api/error_codes/#leaveRequestReceivedError)

```    
class MyChannel extends PhoenixChannel {  
                ........... other code
                @override
                void channelJoined(String completeTopic) { 
                        print("my  error $channelError");
                        // this will try to leave  the channel as soon as it joins
                        leave();

                }
                ............. your other code  
}
```

### **send**<a name="send"></a>

- *return Type* : void
- *parameters* : 
    - required :
        1. String event  
        2. Map<String, dynamic\> payload

This is used to send a quick message to the channel from which it was called without expecting a reply . server may return a reply but if a message was sent from this api then your reply will be given to you in onMessage() callback .
```
class MyChannel extends PhoenixChannel {  
        ........... other code
        @override
        void channelJoined(String completeTopic) { 
                print("my  error $channelError");
                // this will send a message with  ping event and given payload
                // as soon as it joins
                send("ping", {"key1": "value1", "key2": "value2"});

        }
        ............. your other code  
}
```

### **sendForReply**<a name="sendForReply"></a>

- *return Type* : Future<Map<String,dynamic\>?\>?
- *parameters* : 
    - required :
        1. String event  
        2. Map<String, dynamic\> payload
    - optional :
        1. Duration replyTimeOut &nbsp; &nbsp;  default : Duration(seconds :5)  

This is used to send a  message to the channel from which it was called and  expecting a reply . it will return 

1. A future if you are joined in the channel from which this message was sended.
    1. now this future will resolve in map if server sended any response
    2. else if timeout occurs then it will resolve to null .
2. else if you have not joined then it will return null 
```
class MyChannel extends PhoenixChannel {  
        ........... other code
        @override
        void channelJoined(String completeTopic) { 
                print("my  error $channelError");
                // this will send a message with  ping event and given payload
                // as soon as it joins and wait for the reply
                var reply = await sendForReply("ping", {"key1": "value1", "key2": "value2"});

        }
        ............. your other code  
}
```


---
**NOTE**

*if any message was send using [sendForReply](#sendForReply) then its reply will not be received in onMessage callback . It will be received in  future which complete with reply so await on that 
and if you don't want to wait for your reply then use [send](#send) instead which will just send the message and if any reply is sent from the server then you can capture that in onMessage callback*
--- 



