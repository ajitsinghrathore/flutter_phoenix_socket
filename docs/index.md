*flutter phoenix channels* is a package designed to communicate with phoenix channels in backend for realtime experience  

[![Architecture](https://drive.google.com/uc?export=view&id=1p2QU37jUcA8feLjP264k6GTknESkVOuG)](https://drive.google.com/file/d/1p2QU37jUcA8feLjP264k6GTknESkVOuG/view?usp=sharing)

As we can see that every application  will have EndPoints *(WebSocket connections)* and inside every endpoint there are multiple channels through which application will interact to the underlying socket
present in EndPoint.


### General Flow Of communication

1. *Firstly we will have to define endPoint* : Lets consider a chat app so we will define our endpoint by giving necessary details like url to connect .
2. *Then , we will create channels from  EndPoint with the help of **topic** :* 
    1. lets say we have added one channel for topic **chat:myUserId** and any person wants to send message to me will send on this channel.
    2. And lets say we have added one more channel with topic **chat:otherUserId** so that we can send message to that particular  person  via this channel  (assuming that other user have also added this channel)
3. As of now we have only added channels , to communicate  on these channels we must join them by calling join api call on channels.
4. After joining we can send and receive message from that channel


### Package components

1. EndPointsManager : This is a singleton class which will keep track of all the endpoints and you can access any endpoint from this manager . so it is recommended to not keep track of any endPoint object created in your code just register it via addNewEndPoint api call  and access using getEndPoint api call
2. EndPoints : For providing these you will have to create  object of  a class that  extend  from PhoenixEndPoint and register it via add addNewEndPoint api call.
3. channels : For providing  these you will have to create object of class that extends it from PhoenixChannel and add it via phoenixAddNewChannel() api call